---
author: Alain ANDRE
comments: true
date: 2018-01-15 15:19:32 +0200
image: posts/artificial-intelligence.jpg
layout: post
published: true
title: L'intelligence artificielle - définition
categories:
  - intelligence artificielle
tags:
  - ai
  - Artificial intelligence
  - ia
  - Intelligence artificielle
  - taln
  - Traitement automatique du langage naturel
  - nlp
  - Neuro linguistic programming
---

# Objet

# Manipulation de données
Sur Hadoop, Hive : ETL pour pour passer des données d'un format à un autre.

Mahout : Initialement basé sur du mapreduce c'est l'un des premiers outils open-source pour faire du machine learning qui a perdu en popularité avec l'arivée de Spark ML. 

# Références
  - https://keras.io/search.html?q=NLP
  - http://opennlp.apache.org/docs/
  - https://www.tensorflow.org/get_started/get_started
