# Mon site web
Mon site statique personnel généré avec [jekyll](http://jekyllrb.com/) et déployé automatiquement sur [Heroku](https://www.heroku.com/) via [gitlab-ci](http://docs.gitlab.com/ce/ci/quick_start/README.html).

[![build status](https://gitlab.com/al1_andre/mon_site/badges/master/build.svg)](https://gitlab.com/al1_andre/mon_site/commits/master)

# C9

```
bundle exec jekyll serve --host $IP --port $PORT
```

# Images
 - Entête de la page : 900 * 600 px
 - Petite dans le corps du texte : 300 * 250 px
