---
layout: home
title: Home
landing-title: 'Bienvenue sur mon site personnel !'
description: null
image: null
author: null
---

Amoureux de [Ruby on Rails](http://rubyonrails.org/), je suis un développeur passionné, surfant sur l'open source et les nouvelles technologies. Fier utilisateur et défenseur de [Mozilla](mozilla.org), je travaille sur Linux depuis 2003 et suis un fan inconditionnel de [Gnome 3](https://www.gnome.org/gnome-3/)