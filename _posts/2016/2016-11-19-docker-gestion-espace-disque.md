---
author: Alain ANDRE
comments: true
date: 2016-11-19 11:06:16 +0200
description: "Docker est un outil fantastique, mais il ne faut pas oublier une chose. S'il permet de"
image: posts/docker.png
layout: post
published: true
title: "Docker et la gestion de l'espace disque"
categories:
  - docker
tags:
  - space
  - docker
  - disque
---

**Docker** est un outil fantastique, mais il ne faut pas oublier une chose. S'il permet de **lancer** et d'arrêter des **images** aussi **souvent** que l'on souhaite, ça entraîne une **consommation** de l'**espace** disque qu'il ne faut négliger. 

Évidement, étant plutôt **dev.** que système, je ne me suis aperçu du problème qu'une fois que mon **système** a commencer à **hurler** qu'il n'avait plus de place. Et en bon utilisateur que je suis, je me suis dit que je regarderais ça plus tard, c'était tellement improbable que je consomme autant !!

# Ooops I did it again
Le lendemain, j'allume mon ordinateur et là, **impossible** de me **connecter**. J'ai une fois de plus dans mon histoire *tué* mon ordinateur.

Heureusement, utilisateur de Linux depuis plus de 14 ans, un écran noir avec des messages d'insulte ne me fait **pas peur**. Je sais que sous ce système rien n'est jamais mort du moment qu'on a **accès** à un **prompt**. Je me connecte en mode console pour comprendre.

```
~# df
Sys. de fichiers  blocs de 1K    Utilisé  Disponible   Uti%  Monté sur 
udev                 4062232           0     4062232     0%  /dev
tmpfs                 816496        8444      808052     2%  /run
/dev/sdb1          135844024   129152136           0   100%  /  
tmpfs                4082464           0        5120     0%  /dev/shm
tmpfs                   5120           0     4082464     0%  /run/lock
tmpfs                4082464           0     4082464     0%  /sys/fs/cgroup
```

Ok, je ne comprends pas du tout comment j'en suis arrivé là ; c'est **plein à craquer** !! 

Impossible de faire un `| grep` sur une commande, le système étant incapable d'écrire sur `/tmp`.  La **galère** quoi...

J'arrive tout de même avec la commande `du --max-dept=1 / | head -10` à déterminer où se trouve le **problème**. Mon `/var/lib` consomme tout l'**espace**, je rentre dedans et l'examine, ça vient de `/var/lib/docker`.

```
~# du --max-dept=1 /var/lib/docker | head -10
48014064  /var/lib/docker/aufs
104180    /var/lib/docker/volumes
69200     /var/lib/docker/image
4         /var/lib/docker/swarm
4         /var/lib/docker/trust
15404     /var/lib/docker/containers
623560    /var/lib/docker/tmp
64        /var/lib/docker/network
48826496  /var/lib/docker
```

Bon, je commence à comprendre. Je supprime **complètement** `/var/lib/docker` (oui pas de chichi) et j'arrive finalement à retrouver mon **interface** Gnome. Le service Docker a **recréé** toute sons **arborescence** sous `/var/lib` donc c'est super !

# Nettoyage de Docker
Tout d'abord, il convient de **supprimer** régulièrement les **conteneurs** que l'on n'**utilise plus** (plus en ce moment). 

```
alain@alain-Sys1:~/01_projets/docker_al1$ docker ps -aq
CONTAINER ID        IMAGE               COMMAND                   CREATED             STATUS                         PORTS               NAMES
68108367af72        a0effcc62d09        "/bin/sh -c 'rm /tmp'"    12 minutes ago      Exited (1) 12 minutes ago                          suspicious_morse
6ca8188d98aa        c63c09ef4141        "/bin/sh -c 'echo \"no"   12 minutes ago      Exited (1) 12 minutes ago                          condescending_gates
5402c1a1f9b5        79a0d6c83c8f        "/bin/sh -c 'echo \"no"   18 minutes ago      Exited (1) 18 minutes ago                          admiring_stallman
43b45e298c9b        00e5fbab8873        "/bin/sh -c 'echo \"no"   20 minutes ago      Exited (1) 20 minutes ago                          infallible_bardeen
3ab9f8488824        00e5fbab8873        "/bin/sh -c 'echo \"no"   20 minutes ago      Exited (1) 20 minutes ago                          evil_jones
df714b5106dc        client_client       "/docker-entrypoint.s"    35 minutes ago      Exited (6) 35 minutes ago                          client_client_run_17
1e3758eaa8f2        client_client       "/docker-entrypoint.s"    35 minutes ago      Exited (6) 35 minutes ago                          client_client_run_16
21fb940a88df        client_client       "/docker-entrypoint.s"    35 minutes ago      Exited (6) 35 minutes ago                          client_client_run_15
14488b75a5c9        client_client       "/docker-entrypoint.s"    46 minutes ago      Created                                            client_client_run_14
efa9ed094449        client_client       "/docker-entrypoint.s"    About an hour ago   Exited (0) 36 minutes ago                          client_client_run_13
80e0bd814fac        client_client       "/docker-entrypoint.s"    About an hour ago   Exited (1) About an hour ago                       client_client_run_12
767f0804edf5        client_client       "/docker-entrypoint.s"    About an hour ago   Exited (0) About an hour ago                       client_client_run_11
7af58d4f3fc6        client_client       "/docker-entrypoint.s"    About an hour ago   Exited (0) About an hour ago                       client_client_run_10
b4f282b3d52e        client_client       "/docker-entrypoint.s"    About an hour ago   Exited (0) About an hour ago                       client_client_run_9
b39ddb649e1b        client_client       "/docker-entrypoint.s"    About an hour ago   Exited (1) About an hour ago                       client_client_run_8
3c488fa0deb3        client_client       "/docker-entrypoint.s"    About an hour ago   Exited (0) About an hour ago                       client_client_run_7
ed5f9f83c079        client_client       "/docker-entrypoint.s"    About an hour ago   Exited (0) About an hour ago                       client_client_run_6
1b8dcc688924        client_client       "/docker-entrypoint.s"    About an hour ago   Exited (1) About an hour ago                       client_client_run_5
c0617c44556c        client_client       "/docker-entrypoint.s"    2 hours ago         Exited (0) 2 hours ago                             client_client_run_4
``` 

On peut passer à docker une liste d'identifiants à supprimer via `$()`. Ici `awk {'print $1'}` nous permet de ne lister que la **première** colonne de la réponse faite par `docker ps -aq`.

```
alain@alain-Sys1:~/01_projets/docker_al1$ docker rm $(docker ps -aq | awk {'print $1'})
```

Du coup dans la même idée, on peut **supprimer** les **images** qui ne nous servent pas, notamment les **&lt;none&gt;** qui traînent trop souvent lors de *builds* infructueux ou autre. 
L'**id** de l'image est cette fois-ci en **troisième** colonne, donc la commande `awk` est différente : `awk {'print $3'}`.

```
alain@alain-Sys1:~/01_projets/docker_al1$ docker images
REPOSITORY                         TAG                 IMAGE ID            CREATED             SIZE
<none>              <none>              70c0e19168cf        5 days ago          1.069 GB
<none>              <none>              c2ce80b62174        8 days ago          399.2 MB
<none>              <none>              60afe4036d97        8 days ago          325.1 MB
client_app                         latest              488dceb3373d        6 days ago          11.48 GB
al1andre/ionic-2-1-4               android-7-api-24    41ca4c9bc3aa        7 days ago          5.736 GB
al1andre/avd-android-7-api-24      latest              6deede2469c0        7 days ago          5.561 GB
al1andre/ionic                     2.1.4               5fa80038a2b0        7 days ago          402.5 MB
al1andre/extras-android-7-api-24   latest              1920d10ee73a        7 days ago          1.982 GB
al1andre/tools-android-7-api-24    latest              451e9ba989a5        7 days ago          1.446 GB
al1andre/android-7-api-24          latest              8e599d00bc8a        7 days ago          1.43 GB
al1andre/gradle-3-1                latest              5ac98ad6d468        7 days ago          728.5 MB
al1andre/sdk-24                    latest              e2d40b21e90c        7 days ago          1.298 GB
al1andre/jdk-8                     latest              9bb067eb0bce        9 days ago          650.6 MB
ubuntu                             14.04.4             0ccb13bf1954        3 months ago        188 MB
```

On va utiliser la commande `rmi` et lister les images que `grep` va nous filtrer.

```
alain@alain-Sys1:~/01_projets/docker_al1$ docker rmi $(docker images | grep none | awk {'print $3'})
```

# Conclusion
Il vaut mieux régulièrement lister ses conteneurs et en supprimer quelques un. 

Et surtout si votre système vous dit que vous n'avez plus de place, arrêtez ce que vous faites (oui je n'aime pas ça du tout) pour comprendre ce qui se passe avant de galérer de trop :p