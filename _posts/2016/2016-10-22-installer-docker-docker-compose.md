---
author: Alain ANDRE
comments: true
date: 2016-10-22 11:03:16 +0200
description: "Afin de gérer des projets collaboratifs, j'ai besoin que tous les développeurs utilisent le même environnement"
image: posts/docker.png
layout: post
published: true
title: Installer Docker et Docker-compose pour des projets collaboratifs.
categories:
  - docker
  - ruby
tags:
  - ubuntu
  - docker
  - docker-compose
---

Afin de gérer des projets collaboratifs, j'ai besoin que tous les développeurs utilisent le même environnement afin d'éviter les problème de bug générés par les différences de versions. 

Hors certains sont sur des Redhat, d'autres sur OSX ou pire sur du Windows alors que je suis sur Ubuntu. 

Pour un exemple simple de problème lié au système d'exploitation je vous propose de lire cet [article](/ruby/2016/06/20/travailler-avec-des_collaborateurs_windows.html).

# Prérequis 
 - Être sur une machine 64bits.

# Docker
Pour que le projet soit exécuté par tous les contributeurs avec la même version d'environnement, notre projet utilise Docker.

**Pour information** : Docker est un système permettant de gérer (créer/charger/entreposer) des images (comme une VM) mais de façon plus rapide, plus légère ; bref en mieux.

Dans un premier temps, nous devons installer **Docker** en local.  Nous n'allons **pas utiliser** le **repo officiel** d'Ubuntu car la version de **docker-compose** ne supporte pas la version de 2 des yml. 

Pour cela nous tapons `wget -O - https://bit.ly/docker-install| bash`. **Ou** on exécute le **script suivant**.

```
# Ask for the user password
# Script only works if sudo caches the password for a few minutes
sudo true

# Install kernel extra's to enable docker aufs support
# sudo apt-get -y install linux-image-extra-$(uname -r)

# Add Docker PPA and install latest version
# sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9
# sudo sh -c "echo deb https://get.docker.io/ubuntu docker main > /etc/apt/sources.list.d/docker.list"
# sudo apt-get update
# sudo apt-get install lxc-docker -y

# Alternatively you can use the official docker install script
wget -qO- https://get.docker.com/ | sh

# Install docker-compose
COMPOSE_VERSION=`git ls-remote https://github.com/docker/compose | grep refs/tags | grep -oP "[0-9]+\.[0-9]+\.[0-9]+$" | tail -n 1`
sudo sh -c "curl -L https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose"
sudo chmod +x /usr/local/bin/docker-compose
sudo sh -c "curl -L https://raw.githubusercontent.com/docker/compose/${COMPOSE_VERSION}/contrib/completion/bash/docker-compose > /etc/bash_completion.d/docker-compose"

# Install docker-cleanup command
cd /tmp
git clone https://gist.github.com/76b450a0c986e576e98b.git
cd 76b450a0c986e576e98b
sudo mv docker-cleanup /usr/local/bin/docker-cleanup
sudo chmod +x /usr/local/bin/docker-cleanup
```

Une fois installé, nous nous ajoutons au groupe docker.

```
$ sudo addgroup $USER docker
[sudo] Mot de passe de alain : 
Ajout de l'utilisateur « alain » au groupe « docker »...
Ajout de l'utilisateur alain au groupe docker
Fait.
```

Il ne reste plus qu'à se déconnecter/reconnecter (oui je sais c'est pas top) pour s'assurer que docker est opérationnel : 

```
$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```

S'il ne l'est pas, il faut le lancer avec la commande `sudo service docker start`.

*NB* : Pour tout autre système d'exploitation, je vous redirige vers leur [documentation](https://docs.docker.com/engine/installation/).

Dans un second temps, il nous faut installer **Docker compose**.

# Docker compose.
**Pour information** : Si **Docker** permet de charger en quelques secondes la bonne image, docker-compose est ce qui crée la magie. Il nous permet de passer des informations de configuration à notre image et d'utiliser des commandes simples de **build** et autres.

Pour l'installer, on tape la commande suivante `sudo apt-get install docker-compose` pour Ubuntu, pour Windows et Mac vous l'avez déjà dans le bundle que vous avez installé pour Docker. Pour les autre c'est par [là](https://github.com/docker/compose/releases).

# Conclusion
Notre système est prêt !! Nous pouvons maintenant commencer à travailler sur le projet.