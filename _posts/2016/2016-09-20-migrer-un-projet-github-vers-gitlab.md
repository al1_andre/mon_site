---
author: Alain ANDRE
comments: true
date: 2016-09-20 15:25:36 +0200
description: "Gitlab permet en un clic d'importer un projet depuis github, mais qu'en est-il du CI"
image: posts/gitlab.png
layout: post
published: true
title: "Migrer un projet de Github à Gitlab"
categories:
  - bac a sable
tags:
  - continuous integration
  - gitlab
  - github
---

**Gitlab** permet en un clic d'importer un projet depuis **github** donc ce n'est pas un problème du tout. Ce qui est plus problématique c'est le **déploiement automatisé** via Travis.

Gitlab propose **Gitlab-CI** et j'ai donc commencé à regarder comment déployer mon petit site en automatique suite à ma migration vers Gitlab.

# La documentation
Elle est disponible [ici](http://docs.gitlab.com/ce/ci/runners/README.html) et est plutôt bien faite.

# Sélection d'un runner 
Les options de **paramétrage** (settings) sont en haut à droite du projet (la petite roue). Il faut cliquer sur "Runners".

Cette page affiche les différents runners disponibles pour le projet. 

Dans les **specific runners**, j'ai eu la mauvaise experience de tomber sur des runners tournant sur Windows. Donc tout d'abord, 
supprimons tous les **specific runners** disponibles à gauche affin d'utiliser un des **shared runner** pour notre projet. 

Puis, nous allons copier le tocken d'un des **shared runner** qui serra celui utilisé par défaut par notre projet.

# Configuration du runner
Cliquer sur  *CI/CD Pipelines* dans le **paramétrage**. 

Une fois la page affichée, dans la partie **Runners token**, entrons notre tocken précédemment copié.

On voit aussi l'état du build dans la partie **Build status**, copions le code pour l'afficher dans le README du projet.

On enregistre les changements et on va sur la page principale du projet.

# Ajout du fichier yml
Tout d'abord, collons dans le README le code affichant le statut du build.

Puis on va cliquer sur **Set Up CI**. On peut ici sélectionner un code tout prêt pour notre projet selon le code utilisé. Mais avant d'enregistrer, on peut tester la config CI avec ce [lien](https://gitlab.com/ci/lint) ça fait un retour à le mode Swagger.

Si on arrive de Travis, on est un peu **perdu**. Gitlab-CI ressemble plus à du Docker au premier abord. Heureusement ils mettent à disposition des documentations (que l'on peut enrichir) pour la mise en place de tests pour différents langages [ici](http://docs.gitlab.com/ce/ci/examples/).

Je suis donc passé d'un fichier Travis comme celui-ci (avec mon api_key encrypté mais visible)

```yaml
language: ruby
rvm:
- 2.0.0
script: bundle exec jekyll build --source octopress
deploy:
  provider: heroku
  api_key:
    secure: GvMuv4JRP8Bz/nU4Qnk/LyqhN5KhBXJuR4M1Uzb+86HHA6gYSBJSuBgJhv8qz/vadUWDWtPGXXYdGTbV5OfzPmOQczVTkb/zUr0JWHkI8DPcAyRX64CRmWqKtv+PYzdWiEoyhqHmWmCKDdABnK2kAy/VVOsouMQI5/IrRb+ezG32KmmA10hBZe9pPzkh+nU6bmoeYh2Z3IL9moqSaC2Lk7ulzR3FrRG1OhPJGcriL4bgbiEre8x9Oq90KOiwbykHkIFLlBVP6GALVLicnNDrYtCVNJQzEQqHm0PHU1GZcstS4YvBWU0bGK349C8Faj1iwVW6ZAaVoBEPpLrI412gIKo8QBLWlvbB6rlZsLPUI7SOSrkb2dEKSjnEKwXbyibr8FdjxtBlmdirJ1yBBIEyLOl/B3RtchjOlOYBTtYmAx4PIMHX8JyIuvWoLOdP1Fs61OCbwX2XZpOKXFMYQM2Asg1lLv8l4uiZRr7DODu9xF6x8KZPze4vpLfiFv3zVWfQrmSDXkQgx/HR5+fGTTFojlBszshQhG9rnjyDjC1x/JjzZe1/iVgXF/P+k+S3/rBC10ssXrmwNnlj7JfbJqZ/1mPmWYcwr462k2xme2KXR9bw/pea2G470qyfstFj8kUPEhacZ2GBFXB8e3tk38zLSk5fJUoySx5LDk9LHK0jDqo=
  app: mon-site
  on:
    repo: alain-andre/mon_site
```

A ce fichier là avec les variables d'environnement que j'ai enregistré dans le projet sous Gitlab via l'option *Variables* du **paramétrage**. 

**Attention**, récupérez bien la clé sous [Heroku](https://dashboard.heroku.com/account) pour la valeur de **$HEROKU_PRODUCTION_API_KEY** et non celle encryptée qu'il y avait sous Travis.

```yaml

image: "ruby:2.0"

# Cache gems in between builds
cache:
  paths:
    - vendor/ruby
    
before_script:
  - apt-get update >/dev/null
  - apt-get install -y locales >/dev/null
  - echo "en_US UTF-8" > /etc/locale.gen
  - locale-gen en_US.UTF-8
  - export LANG=en_US.UTF-8
  - export LANGUAGE=en_US:en
  - export LC_ALL=en_US.UTF-8
  
test:
  script:
  - ruby -v                                          # Print out ruby version for debugging
  - apt-get update -q && apt-get install nodejs -yqq # rails app needs a JS runtime
  - gem install bundler  --no-ri --no-rdoc           # Bundler is not installed with the image
  - bundle install -j $(nproc) --path vendor         # Install dependencies into ./vendor/ruby
  - bundle exec jekyll build --source octopress

production:
  type: deploy
  environment: production
  script:
  - gem install dpl
  - dpl --provider=heroku --app=$HEROKU_PRODUCTION_APP_NAME --api-key=$HEROKU_PRODUCTION_API_KEY
  - "curl -n -X POST https://api.heroku.com/apps/$HEROKU_PRODUCTION_APP_NAME/ps -H \"Accept: application/json\" -H \"Authorization: Bearer ${HEROKU_PRODUCTION_API_KEY}\""
  only:
  - master
```

# Contrôle des tests
Tout est disponible dans la partie *pipelines* du projet. 

L'interface est **propre**, on y distingue la partie **test** du **déploiement**, les logs sont disponibles. Rien à dire c'est cool.

Ce que j'ai beaucoup aimé c'est l’exécution des tests dans des systèmes encapsulés. Pour mon premier build, j'ai utilisé un runner mutualisé et les tests ne passaient pas alors que sur Travis ils passaient.

```
Conversion error: Jekyll::Converters::Scss encountered an error while converting 'css/main.scss':
                    Invalid US-ASCII character "\xC3" on line 2
```

Après pas mal de recherches et de tests infructueux, j'ai finalement compris que l'image utilisée par mon runner (la **ruby:2.0**) ne disposait pas des encodages UTF-8. Il fallait donc les mettre à disposition comme indiqué dans cette [issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/14983).

# Conclusion
Je ne suis pas un professionnel de Travis, mais ce que propose **Gitlab-CI** est assez **impressionnant** et j'aime beaucoup.
 
 - On peut créer son propre Runner (versionné pour notre prod par exemple).
 - On peut tester la syntaxe de sa configuration. 
 - Les tests sont basés sur des **images** donc versionné.
 - On peut utiliser des variables d'environnement.
 - On peut livrer la **branche** que l'on souhaite dans l'**environnement** voulu. 