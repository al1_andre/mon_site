---
author: Alain ANDRE
comments: true
date: 2016-11-24 15:59:36 +0200
description: "À chaque fois que je cherche à générer un nouveau modèle avec Rails, je recherche"
image: posts/ruby-on-rails.jpg
layout: post
published: true
title: "Les options de génération de modèle de rails."
categories:
  - ruby
tags:
  - generate
  - model
  - scaffold
  - options
  - rails 5
---

À chaque fois que je cherche à générer un nouveau modèle avec Rails, je recherche partout les options disponibles.

Je consacre donc cet article à lister les actions possibles lors de la génération de modèles.

# Génération simple

Aucune difficulté, il suffit de mettre le nom du modèle suivi des attributs que l'on souhaite pour constituer le modèle.

```ruby
rails generate  model user first_name last_name email
```

Ou sur plusieurs lignes.

```ruby
rails generate  model 
         user \
         first_name \
         last_name \
         email
```

# Spécification des types d'attribut

```ruby
rails generate model user username:string
```

La liste des types disponibles est la suivante.

 - integer
 - primary_key
 - decimal
 - float
 - boolean
 - binary
 - string  : de 1 à 255 caractères (255 par défaut)
 - text : de 1 à 4294967296 (65536 par défaut)
 - date
 - time
 - datetime
 - timestamp

# Options

On peut passer des options pour chacun de ces types de la façon suivante. Attention à **decimal** qui doit être encadré par des **'**.

```ruby
rails generate model user username:string{30}
rails generate model product \
  'price:decimal{10,2}' \
  name:string:index  
```

## Lien entre modèles
Il est possible de définir que le modèle **Admin** est issue de **User**.

```ruby
rails generate  model admin --parent user
```

Ce qui génère le modèle suivant.
```ruby
class Admin < User
end
```

## Définir des contraintes

```ruby
rails generate  model user email:index location_id:integer:index
rails generate  model user pseudo:string:uniq
rails generate model user username:string{30}:uniq
```

# Clé étrangères 

```ruby
rails generate model product store:references
```

Ce qui génère le modèle suivant.

```ruby
class Product < ActiveRecord::Base
   belongs_to : store
end
```

Les références polymorphiques.

```ruby
rails generate model picture imageable:references{polymorphic}
```

```ruby
class Picture < ActiveRecord::Base
  belongs_to :imageable, polymorphic: true
end
```

Il reste tout de même à ajouter le `has_many` `as::` dans les modèles liés.

```ruby
class Employee < ActiveRecord::Base
  has_many :pictures, as: :imageable
end
 
class Product < ActiveRecord::Base
  has_many :pictures, as: :imageable
end
```

# Référence
- [polymorphisme](http://guides.rubyonrails.org/association_basics.html#polymorphic-associations).
- [belongs_to](http://guides.rubyonrails.org/association_basics.html#the-belongs-to-association).