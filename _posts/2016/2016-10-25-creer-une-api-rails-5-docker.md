---
author: Alain ANDRE
comments: true
date: 2016-10-25 20:09:36 +0200
description: "L'objectif est de fournir un environnement contenant les composants nécessaires au développement collaboratif"
image: posts/docker.png
layout: post
published: true
title: Creer une api rails 5 à l'aide de docker.
categories:
  - docker
  - ruby
tags:
  - ubuntu
  - docker
  - docker-compose
  - rails 5
  - api
---

L'objectif est de fournir un environnement contenant les composants nécessaires au **développement collaboratif** d'une **API** Rails 5.

# Prérequis 
Installer [Docker et Docker-compose](http://www.alain-andre.fr/docker/ruby%20on%20rails/2016/10/22/installer-docker-docker-compose.html).

# Gemfile
Nous commençons par créer un **Gemfile** contenant les informations minimales.

```ruby
source 'https://rubygems.org'
gem 'rails', '5.0.0.1'
```

Ce fichier va être utilisé par Docker pour constituer un conteneur qui va charger rails.

# Docker
Nous créons ensuite le `Dockerfile` contenant les lignes suivantes.

```
FROM ruby:2.2.2
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /api_server
WORKDIR /api_server
ADD Gemfile /api_server/Gemfile
RUN bundle install
ADD . /api_server
```

**Rails 5** a besoin de **ruby 2.2.2** au minimum, nous nous basons donc sur cette image pour créer notre environnement. 

# Docker-compose
Il nous reste le fichier `docker-compose.yml` que nous allons configurer comme suit. Il va nous permettre de communiquer avec le conteneur qui fait tourner Rails.

```yaml
version: '2'
services:
  db:
    image: postgres:9.6.0
  web:
    build: .
    command: bundle exec rails s -p 3000 -b '0.0.0.0'
    volumes:
      - .:/api_server
    ports:
      - "3000:3000"
    depends_on:
      - db
```

Nous utilisons la **version 2** de **docker-compose** afin de disposer de plus d'options (cf. [compose-file](https://docs.docker.com/compose/compose-file/)).

- `image: postgres:9.6.0` Nous chargeons l'image **9.6.0** de **postgre** afin de toujours disposer du même SGBD sur tous les postes de travail.
- `volumes: - .:/api_server` Nous créons un lien entre le répertoire courant et le répertoire `api_server` de notre conteneur que **Dockerfile** a copié lors du `ADD . /api_server.
- ports:`ports: - "3000:3000"`Nous créons un lien entre notre port et le port de notre conteneur.

# Création du projet Rails

Nous créons maintenant notre projet.

```
docker-compose run web rails new . --api --force --database=postgresql --skip-bundle
```

- `new . ` Nous informons rails de créer le projet dans le répertoire courant.
- `--api` Notre projet doit être une [API-only](http://guides.rubyonrails.org/api_app.html)
- `--database=postgresql` La configuration doit être générée pour **postgre**.

**Rails** vient de nous créer notre projet !! 

Sous **Linux**, il faut changer le détenteur de l’arborescence car **Docker** est lancé en **mode root** donc tous les répertoires sont à lui.

```
sudo chown -R $USER:$USER .
```

# Finaliser l'installation du projet

### CORS
Dé-commenter dans le **Gemfile** la ligne `# gem 'rack-cors'` afin de permettre les requêtes [AJAX inter-origine](https://developer.mozilla.org/fr/docs/HTTP/Access_control_CORS).

Il nous reste à configurer ce que nous autorisons dans le fichier `config/initializers/cors.rb`.

```ruby
Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins '*'

    resource '*',
      headers: :any,
      methods: [:get, :post, :put, :patch, :delete, :options, :head]
  end
end
```

Notre API doit être accessible depuis n'importe où et sur tout type de requête. Pour plus de détail sur ce qui est réalisable, [rack-cors](https://github.com/cyu/rack-cors) explique tout.

Notre **Gemfile** aillant changé, il ne nous reste qu'à **construire** notre projet en le demandant à **docker-compose**.

```
docker-compose build
```

# Faire tourner notre serveur
Rails s'attend à avoir une base de donnée tournant en localhost hors elle est dans le conteneur. Il nous reste à spécifier dans `config/database.yml` le **host**.

L'image de **postgres** initialise un utilisateur dans son [docker-entrypoint.sh](https://github.com/docker-library/postgres/blob/master/9.1/docker-entrypoint.sh) nommé **postgres**, c'est donc celui que nous allons utiliser dans notre **configuration** par **défaut** qui du coup doit ressembler à ceci.

```ruby
  default: &default
  adapter: postgresql
  encoding: unicode
  # For details on connection pooling, see rails configuration guide
  # http://guides.rubyonrails.org/configuring.html#database-pooling
  host: db
  username: postgres
  password:
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
```

Une fois fait, nous allons lancer notre serveur pour la première fois à l'aide de la commande `docker-compose up`.

Nous devons maintenant construire notre base de donnée à l'aide de la commande `docker-compose run web rake db:create`. Vous l'aurez compris, toute commande **rake** peut se faire via **docker-compose**. 

Finalement, nous testons que notre navigateur répond bien à l'adresse [127.0.0.1:3000](http://127.0.0.1:3000) qui nous affiche une jolie image et les informations suivantes.

```
Rails version: 5.0.0.1
Ruby version: 2.2.2 (x86_64-linux) 
```