---
title: Cacher des répertoires du projet lors d'une recherche sous Sublime.
author: Alain ANDRE
description: "Sous Sublime text, il est facile de trouver un fichier grâce à la commande [CTRL][P]"
image: posts/sublime-text.png
layout: post
comments: true
published: true
date: 2016-10-21 16:25:06 +0200
categories:
  - bac a sable
tags:
  - sublime text
  - cacher
  - CTRL P
---

# L'Objet
Sous [Sublime text](http://sublimetext.com), il est facile de trouver un fichier grâce à la commande **[CTRL][P]** qui propose les fichiers correspondants à notre recherche depuis toute l'arborescence d'un projet. 

# La problématique
Cependant, il n'est pas toujours intéressant d'avoir dans cette liste les fichiers se trouvant dans certains répertoires. Par exemple si je cherche `models mon_model` j'aimerais que tous fichier de la documentation technique soient exclus de ma recherche.

# La solution
Il est possible de configurer son éditeur afin d'exclure certains types de fichiers ou répertoires de la façon suivante.
  
## Pour Sublime 2

Soit dans le fichier de projet `projet.sublime-project`, soit dans les configurations utilisateur *Preferences*/*Settings - User*, ajouter le code suivant.
  
```
"binary_file_patterns": ["*.jpg", "*.jpeg", "*.png", "*.gif", "*.ttf", "*.tga", "*.dds", "*.ico", "*.eot", "*.pdf", "*.swf", "*.jar", "*.zip"],
"folder_exclude_patterns": [".svn", ".git", ".hg", "tmp"],
```
  `binary_file_patterns` retire de la recherche les fichiers dont les extensions sont listées alors que `folder_exclude_patterns` va retirer les répertoires.

  Malheureusement, ces répertoires sont aussi retirés de la **liste des éléments** (la partie à gauche).

## Pour Sublime 3 

Soit dans le fichier de projet `projet.sublime-project`, soit dans les configurations utilisateur *Preferences*/*Settings - User*), ajouter le code suivant.


```
"binary_file_patterns": ["*.jar", "*.zip", ".svn", ".git", ".hg", "tmp/"]
```

En mettant les **répertoires** dans **binary_file_patterns**, Sublime va les retirer des recherches mais les laisser dans la **liste des éléments**. 