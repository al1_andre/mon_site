---
author: Alain ANDRE
comments: true
date: 2016-09-27 10:39:06 +0200
description: "Docker ne supporte pas officiellement le 32bit. Résultat des courses, les projets ne"
image: posts/docker.png
layout: post
published: true
title: Créer une image Docker de ionic 2 sur Ubuntu 32bit
categories:
  - docker
tags:
  - docker
  - ubuntu
  - 32bit
---

Docker ne supporte pas officiellement le **32bit**. Résultat des courses, les projets ne proposent pas de version 32bit et mon petit portable ne m'est pas très utile pour développer mon projet. J'ai donc cherché à créer ma **propre image** pour un Ionic 2 qui tourne sur ma machine.

Il y a un utilisateur [docker-32bit](https://github.com/docker-32bit) sur Github qui propose un shell pour générer une image d'Ubuntu en 32bit ; ça tombe bien, on va commencer par ça.

```
git clone https://github.com/docker-32bit/ubuntu
cd ubuntu
./build-image.sh
```

Une fois l'image créée, on vérifie qu'elle est bien enregistrée dans Docker 

```
$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
32bit/ubuntu        xenial              b3b4a42c1931        26 minutes ago      321.1 MB
```

Testons si notre installation est bonne. En tapant  `docker run -it 32bit/ubuntu:xenial /bin/echo 'salut !'`, le prompt devrait nous afficher un jolie **salut !**.

On va maintenant pouvoir lancer notre **image** Ubuntu dans un **container**, modifier ce qui s'y trouve et l'enregistrer.

```
docker run -it 32bit/ubuntu:xenial /bin/bash
```

La commande docker ci-dessus demande de lancer l'image dont le répertoire est **32bit/ubuntu** et le tag **xenial** puis de nous ouvrir le bash.

L'utilisateur du prompt doit avoir changé en quelque chose du genre `root@0c748c71fe84`.

On liste ce qui s'y trouve.

```
root@0c748c71fe84:/# ls -la
total 68
drwxr-xr-x  26 root root 4096 Sep 26 16:39 .
drwxr-xr-x  26 root root 4096 Sep 26 16:39 ..
-rwxr-xr-x   1 root root    0 Sep 26 16:39 .dockerenv
drwxr-xr-x   2 root root 4096 Sep 26 16:20 bin
drwxr-xr-x   2 root root 4096 Apr 12 20:14 boot
drwxr-xr-x   5 root root  380 Sep 26 16:39 dev
drwxr-xr-x  63 root root 4096 Sep 26 16:39 etc
drwxr-xr-x   2 root root 4096 Apr 12 20:14 home
drwxr-xr-x  11 root root 4096 Sep 26 16:19 lib
drwxr-xr-x   2 root root 4096 Sep 26 16:13 media
drwxr-xr-x   2 root root 4096 Sep 26 16:13 mnt
drwxr-xr-x   2 root root 4096 Sep 26 16:13 opt
dr-xr-xr-x 267 root root    0 Sep 26 16:39 proc
drwx------   2 root root 4096 Sep 26 16:13 root
drwxr-xr-x   7 root root 4096 Sep 26 16:20 run
drwxr-xr-x   2 root root 4096 Sep 26 16:19 sbin
drwxr-xr-x   2 root root 4096 Sep 26 16:13 srv
dr-xr-xr-x  13 root root    0 Sep 26 16:39 sys
drwxrwxrwt   2 root root 4096 Sep 26 16:20 tmp
drwxr-xr-x  10 root root 4096 Sep 26 16:13 usr
drwxr-xr-x  11 root root 4096 Sep 26 16:13 var
```

On fait une petite mise à jour.

```
apt-get update
```

# Installation de nodejs et npm.

A l'heure où j'écris, **Ionic v2** ne [supporte pas](http://ionicframework.com/docs/v2/getting-started/installation/) encore **Nodejs v6**, il faut donc installer la version 5 avec la version 3 de **npm**.

```
apt-get install curl
curl -sL https://deb.nodesource.com/setup_5.x | bash -
apt-get install nodejs
```

On vérifie maintenant les installations avec leurs versions.

```
root@0c748c71fe84:/# node -v
v5.12.0
root@0c748c71fe84:/# npm -v
3.8.6
```

On installe alors **Ionic** v2 et **Cordova** v6.3.

```
npm install -g ionic@beta
npm install -g cordova@6.3.1
```

Il reste à installer un programme très important pour toute gestion de projet, j'ai nommé **git**.

```
apt-get install git
```

On peut maintenant demander à Docker de nous créer une **image** du **container** dans lequel on se trouve. Sortons d'abord du **container** : `$: exit`

Listons maintenant les **containers** pour retrouver l'ID qui nous intéresse. 

```
$: docker ps -a
CONTAINER ID        IMAGE                 COMMAND                  CREATED              STATUS                         PORTS                NAMES
aac6dd43321f        32bit/ubuntu:xenial   "/bin/bash"              About a minute ago   Up About a minute                                   suspicious_franklin
f5773563d2ad        32bit/ubuntu:xenial   "/bin/echo 'salut !'"    About an hour ago    Exited (0) About an hour ago                        elated_bassi
```

Le **container** qui nous intéresse est le **aac6dd43321f**, on tape donc la commande suivante.

docker commit -a '*Votre Nom* <*votre@dresse*>' -m "*un petit descriptif*" *CONTAINER_ID* *nom/image*

```
$ docker commit -a 'Alain ANDRE <dev@alain-andre.fr>' -m "Ionic2 sur Ubuntu 16.04 32bit" 0c748c71fe84 al1andre/ionic2-32b
sha256:2999b5ea756a2c3da470fe6077672ec5b143f471f51477b3ecde634a28860af6
```

La commande s'est bien passée, nous pouvons constater qu'une nouvelle image est disponible dans notre docker.

```
$ docker images
REPOSITORY            TAG                 IMAGE ID            CREATED             SIZE
al1andre/ionic2-32b   latest              2999b5ea756a        3 minutes ago       652 MB
32bit/ubuntu          xenial              b3b4a42c1931        About an hour ago   321.1 MB
```

Nous pouvons maintenant lancer notre image contenant Ubuntu Xenial 32bit NodeJs v5, NPM v3, Ionic v2 et cordova v6.

```
$ docker run -it al1andre/ionic2-32b /bin/bash
root@86eb6f1f3181:/# node -v
v5.12.0
```

Il nous est possible maintenant de déposer notre **image** sur le hub de docker en suivant [cette](https://docs.docker.com/engine/tutorials/dockerrepos/) documentation.

De cette façon tous les collaborateurs peuvent créer un alias après avoir chargé l'image et utiliser le même ionic.

```
alias ionic="docker run -ti --rm -p 8100:8100 -p 35729:35729 -v \$PWD:/MyApp:rw al1andre/ionic2-32b:latest ionic"
ionic start MyApp --v2
cd MyApp
ionic serve --lab -b # http://localhost:8100/ionic-lab
```