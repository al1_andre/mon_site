---
author: Alain ANDRE
comments: true
date: 2018-03-31 10:19:32 +0200
image: posts/artificial-intelligence-humain.png
layout: post
published: true
title: L'intelligence artificielle et l'humain
categories:
  - intelligence artificielle
tags:
  - ai
  - Artificial intelligence
  - ia
  - Intelligence artificielle
  - alphago
  - deepmind
---

<img alt="Fan hui" src="{{site.image_baseurl}}posts/fanhui.jpg" class="left-image" /> Mon dernier [article](/intelligence%20artificielle/2018/02/27/intelligence-artificielle-definition.html) présente certains des aspects les plus prometteurs de l'Intelligence artificielle (c'est d’ailleurs confirmé par le [rapport de Cédric Villani](https://www.aiforhumanity.fr/) rendu ce 28 mars 2018) et je tenais avant de réaliser notre première IA à parler de ce qu'elle représente aussi pour l'humain, pour nos sociétés, notre travail. Quasi tout le monde se pose la question aujourd'hui : l'IA va-t-elle prendre ma place ?

Pour proposer un élément de réponse, je vais parler des premiers artistes a y avoir été confrontés. J'ai eu l'honneur de rencontrer [Fan Hui](https://fr.wikipedia.org/wiki/Fan_Hui) au [Node](https://www.aquinum.fr/le-node/c-est-quoi-le-node.html) de Bordeaux lors du [meetup](https://www.meetup.com/fr-FR/Bordeaux-Machine-Learning-Meetup) de [Louis Dorad](http://www.louisdorard.com/) sur le Machine-Learning. Cet homme est le premier professionnel de jeu de GO (numéro un Européen) a avoir perdu face à l'Intelligence Artificielle AlphaGo créée par [Deepmind](https://deepmind.com/) dont le co-fondateur [Demis Hassabis](https://en.wikipedia.org/wiki/Demis_Hassabis) a participé aux débats proposés à la journée [AI for Humanity](https://www.aiforhumanity.fr/)

# Le jeu de Go
Pour bien comprendre l'enjeu et la violence de cette défaite, il faut savoir qu'en Asie, les joueurs de go sont considérés comme des artistes et de grands sages car ce jeu millénaire est en quelque sorte une approche philosophique de la vie.

Voici les règles établies par Confucius il y a 2 500 ans :

 - Le plateau du jeu de go (goban) est composé de 19 lignes (de vie) verticales et 19 horizontales ;
 - Les deux joueurs doivent placer, chacun leur tour, un pion (historiquement une pierre) blanc ou noir sur une intersection ;
 - Ces pions sont tous les mêmes et ne peuvent pas être bougés une fois posés ;
 - Ils peuvent par contre être capturés lorsqu’ils sont entièrement encerclés ;
 - Le but du jeu est de contrôler le plus d'ensembles de pions ou d'intersections vides, ne pouvant pas être prises par l'adversaire.

Ces règles sont "trop simples" ce qui engendre des concepts trop abstraits pour une machine. De plus, elles offrent un potentiel de combinaisons de jeu astronomique soit 10 puissance 170 (prenez un 1 et ajoutez lui 170 zéros) c'est pourquoi aucun ordinateur ni supercalculateur au monde ne peut aujourd'hui calculer toutes les combinaisons lors d'un match. Les calculateurs aux jeux d’Échec sont basés sur la valeur des pièces (pion, tour, reine etc.) pour déterminer l'état d'avancement du jeux et les risques ; ce qui n'est pas possible au Go car toutes les pierres ont la même valeur, et c'est leur position sur le goban qui leur donne une importance propre !

# Fan Hui
<img alt="Fan Hui" src="{{site.image_baseurl}}posts/fan-hui-deepmind.jpg" class="full-image" /> Dans le monde du Go, les ordinateurs sont considérés comme de vulgaires bouts de ferrailles tout juste bon à servir de punching-ball aux débutants. Imaginez donc l'impact de la défaite de Fan Hui face à AlphaGo dans leur siège sociale à Londres ! Il relate que lorsqu'il est rentré chez lui à Bordeaux, son épouse lui a demandé de ne pas regarder les réseaux sociaux, de ne pas aller sur Internet... L'Asie entière se moquait de lui en disant qu'il vivait en Europe depuis tellement longtemps qu'il ne savait plus jouer au Go ; rappelons que Fan était alors champion d'Europe !

DeepMind avait réussi à battre un champion européen, il fallait maintenant prouver à tous qu'ils étaient capable de battre le champion du monde. L'équipe de Londres avait beaucoup apprécié l'état d’esprit de Fan, ils l'ont donc contacté pour savoir s'il pouvait travailler avec eux. Et quelle fut sa réponse ? Oui ! Plutôt que de céder à la peur et au rejet, c'est avec humilité et force qu'il s'est intéressé au sujet et a décidé d'y participer. Fan Hui a travaillé à l'amélioration du programme, il a aussi été l'arbitre officiel lors de la rencontre avec le champion du monde [Lee Sedol](https://fr.wikipedia.org/wiki/Lee_Sedol). Vous pouvez le découvrir dans l'excellent [film](https://www.netflix.com/title/80190844) qui est sorti sur Netflix dont vous avez pu voir la bande d'annonce dans mon [article]({{site.url}}/intelligence%20artificielle/2018/01/28/intelligence-artificelle-en-france.html) sur l'IA en France.

# Lee Sedol
<img alt="Lee Sedol" src="{{site.image_baseurl}}posts/leesedol-1match.png" class="full-image" /> La rencontre de Lee Sedol et d'AlphaGo fut un moment historique. Le jeu de Go est joué par plus de huit millions de personnes en Corée et leur star nationale, Lee, domine le monde depuis plus de dix ans. Avant la première partie, les pronostics des professionnels étaient unanimes : 5-0 pour la star coréenne. Mais après la première défaite du champion, leur regard a changé, à l'image de ces commentateurs coréens incrédules sur la photo de gauche après la première défaite.

Les joueurs de Go n'en croyaient pas leurs yeux de voir leur héros perdre la première puis la seconde et la troisième manche. Le mal-être était perceptible, le monde entier ne disait plus un mot ; même l'équipe de DeepMind ressentait une énorme tristesse. Cet homme (le meilleur d'entre nous) donnait sont maximum mais s'enfonçait jeu après jeu au point de s'excuser publiquement de ne pas être assez intelligent pour battre l'IA. Cette troisième défaite, Lee Sedol devint l'Humanité se battant contre sa création.

# Conclusion
{% youtube 'https://www.youtube.com/watch?v=QT8z4mW1ZKc' %} Lors de la victoire de Lee Sedol lors de la quatrième manche, c'est, comme le dit ce commentateur sur Arirang News, une victoire pour l'Humanité. En quelques jours, tout le monde s'est approprié cette victoire comme un espoir sur notre capacité à battre une IA si forte dans un domaine sur lequel on ne l'attendait pas.

Après avoir vu ce film et discuté avec Fan, je retiens de ces deux champions une vision incroyablement optimiste. Après la victoire d'AlphaGo en Corée, les joueurs de Go plutôt que de se sentir abattus, humiliés ou désemparés ont utilisés l'IA pour s'améliorer ; le jeu a connu un intérêt international nouveau et le nombre de joueur a augmenté !

Lee Sedol s'est senti grandi et reconnaissant de cette expérience, il dit aujourd'hui avoir trouvé sa raison de jouer au GO ! Alors que tout le monde le pensait à son maximum, cette rencontre avec l'IA lui a appris qu'il pouvait faire plus encore. Les deux mois qui ont suivi sa rencontre avec AlphaGo, Lee a gagné tous les tournois qu'il a joué !

Fan Hui malgré sa défaite contre une IA, a accepté de participer à son amélioration - qui d'entre nous en serait capable ? Cet homme a un mental incroyable ; c'est d'ailleurs l'un des deux seuls joueurs au monde a avoir battu AlphaGo. Il dit être sorti de cette expérience en homme grandi et plein d'espoir sur ce que les IA peuvent nous apporter. En effet, pour lui, l'IA est un outil fantastique d'apprentissage et d'aide au développement personnel c'est la raison pour laquelle il a aussi participé à la création d'[alphagoteach](https://alphagoteach.deepmind.com/) un outil d'apprentissage basé sur des milliers de parties humaines qui propose l'analyse en direct de plus de six mille ouvertures.

Dans mon prochain article je vais présenter comment il est possible pour tout un chacun d'utiliser une reconnaissance d'image sans avoir besoin d'être des développeurs ou des génies des mathématiques !
