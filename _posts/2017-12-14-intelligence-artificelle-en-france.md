---
author: Alain ANDRE
comments: true
date: 2018-01-06 15:19:32 +0200
image: posts/france-ia.png
layout: post
published: true
title: L'intelligence artificielle en France
categories:
  - intelligence artificielle
tags:
  - ai
  - Artificial intelligence
  - ia
  - Intelligence artificielle
  - taln
  - Traitement automatique du langage naturel
  - nlp
  - Neuro linguistic programming
---

Cet article est une introduction à mon projet personnel pour 2018.

# Objet
J'entends de plus en plus parler de l'**intelligence artificielle** et des avancées sidérantes des **GAFA** (Google, Apple, Facebook et Amazon) , des **NATU** (Netflix, Airbnb, Tesla et Uber) et des **BATX** chinois (Baidu, Alibaba, Tencent et Xiaomi) dans ce domaine et ça m'intéresse beaucoup. Je souhaite en **apprendre** plus mais si l'Internet foisonne d'informations, il faut tout de même savoir la chercher. Il y a de nombreux articles en **Anglais** mais peu en **Français** alors que les concepts ne sont pas simple de prime abord. Je souhaite donc apporter ma pierre à l'édifice en **partageant** ce que j'apprends.

# Description
> On regroupe habituellement sous le terme d'intelligence artificielle un ensemble de notions s'inspirant de la cognition humaine ou du cerveau biologique, et destiné à assister ou suppléer l'individu dans le traitement des informations massives. (wikipedia)

L'intelligence artificielle est une discipline ancienne dans l'informatique. Le concept est né avec **Leibniz** au XVIIs, est entrée dans le domaine de la science avec **Alan Turing** et son test en 1950 pour se voir établi lors d'une conférence tenue au campus de **Dartmouth College** en 1957. Quelque peu abandonnée au fil du temps, c'est seulement depuis peu qu'elle refait surface dans les médias grâce à l'une de ses méthodes : l'apprentissage profond (**Deep Learning**) dont le docteur [Yann Le Cun](https://fr.wikipedia.org/wiki/Yann_LeCun) fut l'un des précurseurs dans les années 1980 et qui aujourd'hui dirige le laboratoire d'intelligence artificielle de Facebook.

En janvier **2017**, le **secrétaire d’État chargé de l'Industrie, du Numérique et de l'Innovation** et le **secrétaire d’État chargé de l'Enseignement supérieur et de la Recherche** ont lancé la démarche **#franceIA** qui avait pour objectif d'étudier les opportunités de l'**IA**. Le rapport de synthèse de cette étude est disponible [ici](https://www.economie.gouv.fr/files/files/PDF/2017/Rapport_synthese_France_IA_.pdf).

L'intelligence artificielle traite des domaines suivants :

<img alt="Les domaines de l'IA" src="{{site.image_baseurl}}posts/domaines-ia.jpg" class="full-image" />

# Pourquoi écrire en français ?
La France a-t-elle déjà presque perdu la guerre de l'IA ? C'est l'un des sujets du livre [La Guerre des intelligences](https://www.mollat.com/livres/2088679/laurent-alexandre-la-guerre-des-intelligences-comment-l-intelligence-artificielle-va-revolutionner-l-education) écrit par le **docteur Laurent Alexandre**. Si les **Français** se trouvent assez **bien représentés** en ce qui touche à l'Intelligence Artificielle comme le dit le rapport du *gouvernement Hollande*, les **articles** en Français sur Internet sont vieux, vide de sens des fois, trop **compliqués** souvent ; mais surtout **jamais sexy** ! L'IA semble, pour le commun des mortels, **intouchable**, **stratosphérique** et même pour certains considérée à surtout éviter, voir **dangereuse** pour l'humanité ! Or il y a un **enjeu** national de taille. Nous avons **raté** le passage à l'**Internet** parce que nous pensions être les **maîtres du monde** avec notre **minitel** que nous n'avons pas fait évoluer ; il s'agit là de ne pas manquer la marche -déjà bien grandie par des années d'inaction- en pensant que fermer les yeux nous évitera d'être dominés par une IA !

{% youtube "https://www.youtube.com/watch?v=8tq1C8spV_g" %} Une chose extrêmement importante à comprendre dans le **fonctionnement** de l'IA est que le **code** n'est pas le plus important aspect de cette intelligence ; c'est la **donnée** qui la constitue ! C'est là une barrière conceptuelle importante car il s'agit d'une vision quasi inverse des programmes que tout un chacun connaît. [AlphaGo](https://fr.wikipedia.org/wiki/AlphaGo) de [DeepMind](https://deepmind.com/) par exemple, n'a pas gagné contre le champion du monde [Lee Sedol](https://fr.wikipedia.org/wiki/Lee_Sedol) en calculant toutes les possibilités de coups (un super calculateur), mais en se basant sur son expérience acquise (les données) comme un joueur l'aurait fait.

Et si à l'heure où j'écris, les **infrastructures** françaises sont quasi **inaccessibles**, nos **ingénieurs** se **vendent** bien dans les entreprises étrangères ! Si nous étudions l'IA, si nous en parlons ; si nous écrivons des articles scientifiques, c'est en **anglais** ! Or dans les enjeux de l'IA, on compte l'**économie**, la **santé**, l'**industrie**, la **défense** qui ont des dimensions **humaines** fortes. La majorité des personnes n'ont pas accès aux articles scientifiques, si l'on considère l'IA comme une discipline de quelque élite, si l'on **alimente** les IA avec seulement certains types de **données** et de **concepts**, la perte de notions importantes est inévitable. Un très bon exemple est le chat-bot [Tay](https://techcrunch.com/2016/03/24/microsoft-silences-its-new-a-i-bot-tay-after-twitter-users-teach-it-racism/) mis en place par Microsoft pour répondre aux internautes qui, au contacte des réseaux sociaux, est devenu profondément antisémite, raciste et xénophobe !

L'IA va rapidement **devenir** un outil d'**aide** à la prise de décision dans les **entreprises**, dans la **société** ainsi que dans notre **quotidien**. En fait, c'est **déjà** le cas pour vos **recherches** sur Internet, vos **itinéraires** ou votre **sécurité** en voitures, la gestion de **vos données** sur les **clouds** (hébergements synchronisés) etc. Lorsque **vous** répondez à un captcha afin de savoir si vous êtes bien un **humain**, vous pensez vraiment que **Google** ne vous utilise pas pour **former** une IA ? Si **Skynet** vous dit quelque chose, je vous laisse comprendre l'**importance** d'une **indépendance** de l'IA ou tout du moins d'une IA **alliée**.

# Mon objectif
Aujourd'hui, la langue **universelle** de la **science** et de la **technique** est bel et bien l'**anglais**. lorsque je développe, tout mon code est en anglais ainsi que ses commentaires ; c'est ce qui permet à une communauté internationale de **partager** et d'améliorer de façon rapide et compétitive ses produits. On parle alors d'**open-source** et de **logiciels libres**, je suis persuadé que le partage du code est ce qui le rend plus fort et plus **sûr**. Mais comme le soulève le **rapport de synthèse de France IA** de 2017, la France **manque** cruellement de **tutoriels** et de cours abordables en ce qui concerne l'IA ; mon **idée** est de proposer des explications en français sous forme de tutoriels **accessibles**, **ludiques** et **agréables**.

Si j'ai appris une chose au fil de mes années d’**expériences** de développeur, c'est qu'il ne faut pas **réinventer** la roue ! Si on a une idée, quelqu'un dans le monde l'a très probablement **déjà eu**, et il vaut mieux **réutiliser** son travail. La **difficulté** est de le trouver et d'en avoir les **droits**. C'est d’ailleurs l'une des problématiques principale de l'IA ; comme le dit **Mozilla** : il n'y a pas assez de donnée open-source disponible pour des IA libres. Pour le Docteur **Laurent Alexandre** c'est une problématique européenne : la loi encadrant la donnée ne permet pas d'avoir **suffisamment** de données pour **entraîner** nos IA comparé à la **Chine** ou les **États Unis**.

{% youtube "https://www.youtube.com/watch?v=jzi1fpi3XHg" %} Par exemple, **Facebook** met à disposition des outils pour jouer avec son IA [wit](https://wit.ai/) qui permet, entre autre, de créer des chat-bot (réponses automatisées sur les réseaux sociaux ou les sites de e-commerce) mais tout ou presque leur appartient ! Autrement dit, pendant que **vous entraînez** votre bot, vous entraînez surtout **le leur**. **Amazon** aussi propose ses services d'IA via son [portail](https://aws.amazon.com/fr/machine-learning/) avec un allier de poids : [Nvidia](http://www.nvidia.fr/object/ai-computing-fr.html) et sa GPU (mémoire vive de la carte graphique). J'ai trouvé, après avoir passé **pas mal de temps** à chercher des **tutoriels** et des **bibliothèques** libres, un très bon site qui permet de faire de l'IA : [scikit-learn](http://scikit-learn.org) dont l'[INRIA](https://www.inria.fr/) est un des moteurs ainsi que [pytorch](http://pytorch.org/) qui est bien plus évolué et permet comme [tensorflow](https://www.tensorflow.org/) de faire du deep-learning et d'utiliser la **GPU**.

L'Intelligence artificielle est un sujet **sensible** mais pourtant **nébuleux** autant pour ceux qui travaillent dans l'informatique que pour les autres. Aussi, à l'heure où j'écris, il n'y a à ma connaissance, que l'[OpenIA](https://openai.com/) d'[Elon Musk](https://fr.wikipedia.org/wiki/Elon_Musk) ainsi que le tout nouveau projet [ DeepSpeech](https://blog.mozilla.org/blog/2017/11/29/announcing-the-initial-release-of-mozillas-open-source-speech-recognition-model-and-voice-dataset/) de [Mozilla](http://mozilla.org/) (utilisant [tensorflow](https://www.tensorflow.org/)) qui se veulent universels et **libres**. Si le profit est bon, il ne doit être qu'un **résultat** et non un **objectif** ! Quel va être l'impact des IA des GAFAM sur nos sociétés ? partageront-ils leur savoir ?

# Conclusion
> L’intelligence artificielle est l’affaire de tout le monde. (Cédric Villani)

Le 22 janvier 2018, Facebook et Google annonçaient à l'occasion du sommet "**Choose France**" organisé par Emmanuel Macron qu'ils choisissaient la France. **Facebook** [investit](https://www.sciencesetavenir.fr/high-tech/intelligence-artificielle/facebook-mise-10-millions-d-euros-en-france-sur-l-intelligence-artificielle_120115) pour **10M€** dans son centre de recherche fondamentale européen qui se trouvera à Paris où **Google** y [installe](https://www.silicon.fr/intelligence-artificielle-google-recherche-france-197193.html?inf_by=5a67afd8681db81b528b47a8) aussi le sien qui va travailler sur la santé, les sciences, l'art ou encore l'environnement et dont les résultats seront rendus open-source. De plus, Google va ouvrir quatre centres de formation dans toute la France dont l'objectif est de former 100 000 personnes par an.

Vous l'aurez compris, nous avons les **capacités**, nous devons nous donner les **moyens**. Je vais, dans mon prochain [article](/intelligence%20artificielle/2018/02/27/intelligence-artificielle-definition.html), donner une définition simple et intelligible de l'**apprentissage automatique**, puis je rentrerais dans les détails en partant sur la **création** d'une intelligence artificielle. Étant littéraire et passionné de langues, je vais partir sur le **traitement automatique du langage naturel**. N'hésitez pas à partager et à commenter si cet article vous a intéressé.
