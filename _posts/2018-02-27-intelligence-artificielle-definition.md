---
author: Alain ANDRE
comments: true
date: 2018-02-27 15:19:32 +0200
image: posts/artificial-intelligence.jpg
layout: post
published: true
title: L'intelligence artificielle - définition
categories:
  - intelligence artificielle
tags:
  - ai
  - Artificial intelligence
  - ia
  - Intelligence artificielle
  - voiture autonome
  - imagerie intelligente
---

Après le dernier [article](/intelligence%20artificielle/2018/01/06/intelligence-artificelle-en-france.html) sur l'importance de l'Intelligence Artificielle (IA) en France, je propose ici une définition que je trouve la plus abordable et la plus complet possible.

# À propos

Tout d'abord, il est important de préciser que l'Intelligence artificielle n'a rien de magique. Elle est constituée de techniques qui doivent répondre à des objectifs précis. Pour cette raison, elle ne peut être transverse et donc se comparer à l'**intelligence** générale qui est la capacité à choisir la bonne **intelligence** pour la bonne action. Une IA qui fait de l'imagerie médicale ne saura pas détecter des âges sur une photo et inversement. Ce que fait l'IA, elle ne le fait pas mieux mais plus vite que l'humain et c'est tout son intérêt.

## La santé

{% youtube 'https://www.youtube.com/watch?v=uHPZ7eLJyJc' %} Les portées sont colossales, par exemple dans l’aide au diagnostic médical, l'IA permet d'assister les analystes dans la recherche de maladies, elle peut traiter des milliers de dossiers pour ne signaler aux docteurs que les cas les plus graves ou sur quelles zones d'intérêt se concentrer.

Elle peut aussi prédire avec une marge d'erreur réduite certains type de cancers, de maladies génétiques etc. Nous serions capable de faire de la médecine prédictive tout en soulageant les équipes qui tous les jours travaillent sur nos dossiers en leur permettant d'améliorer la prise en charge du patient.

C'est pour ces raisons par exemple que la start-up Israélienne [A.I. Doctor](https://www.snapmunk.com/israeli-startup-media-ai-radiology-funding-aidoc/) a réussi à lever sept millions de dollars l'année dernière.

## Les transports

{% youtube 'https://www.youtube.com/watch?v=vmgDAa1Fy7E' %} Un autre exemple d'application pratique de l'IA est celle qui en est faite dans les voitures autonomes dont on entend de plus en plus parler dans la presse : [Waymo](https://www.tomsguide.fr/actualite/waymo-google-voiture-autonome,61176.html) de Google, [Tesla](https://www.tomsguide.fr/actualite/tesla-intelligence-artificielle-voitures-autonomes,60420.html), [Apple](https://www.numerama.com/tech/324756-apple-fait-desormais-rouler-27-voitures-autonomes-en-californie.html), la [RATP](http://www.leparisien.fr/paris-75/paris-un-nouveau-minibus-sans-chauffeur-teste-par-la-ratp-20-01-2017-6595302.php) à Paris, le [Navya](http://navya.tech/) à Lyon (pour ne citer qu'eux).

Ces véhicules sont capables d'identifier sur les images filmées à la volée, les être vivants, les panneaux de signalisation en passant par les routes sans lignes blanches et les objets en circulation autour ; ce qui leur permet d'agir en conséquence !

Tout ceci est possible grâce aux incroyables capacités de calculs des processeurs modernes mais aussi aux techniques algorithmiques et aux bases de données d'images chaque jours plus importantes.

# Apprentissage Automatique - Machine Learning

<img alt="Detection de chat" src="{{site.image_baseurl}}posts/machine-learning.png" class="full-image" />
Le Machine Learning est une méthode créée dont l'objectif est d'éviter les règles métiers qui polluent le code et les bases de données. C'est la réponse à une problématique que tout développeur a déjà rencontré dans son parcours. Pour nous, il s'agit de la création d'un algorithme minimal qui permet de générer des associations de données créant des modèles de règles métier par l'apprentissage.

L'une de ces techniques algorithmiques s'appelle l'apprentissage automatique, elle permet à l'**intelligence** artificielle d'apprendre ! C'est à dire de réaliser une action de façon différente de la dernière qui n'avait pas abouti au résultat souhaité : d’apprendre de ses erreurs !

Pour apprendre, il faut faire des exercices dont on connaît le résultat, et  donc, avant de créer une Intelligence Artificielle, il faut se poser bien des questions. Alors, en prenant pour exemple d'apprentissage "la reconnaissance d'un chat", il faudra se demander :

 - A quelle question dois-je répondre (Par exemple déterminer sur une image, s'il s'agit d'un chat)
 - Comment y répondre - avec quel type de données (des images)
 - Où puis-je trouver ces données (Sur Facebook, le chat est l'image la plus répandue)
 - Quels sont mes paramètres d'ajustement (Quatre pattes, deux yeux, une queue etc.)

Et mener les actions nécessaires :

 - Structurer les données d'apprentissage (Pour chaque image, savoir s'il s'agit d'un chat ou non)
 - Créer les modèles permettant d'ajuster les paramètres (Lors d'erreur de diagnostiques)
 - Traiter les données structurées dans les modèles (Faire visualiser des millions d'images de chats)
 - Tester le modèle avec des données structurées autres que celles utilisées pour l'apprentissage (Montrer un guépard)


Les deux principaux objectifs du Machine Learning sont la prédiction et la classification.

## 1. La prédiction

<img alt="Régression linéaire" src="{{site.image_baseurl}}posts/linear-regression.png" class="left-image" />C'est un modèle mathématique de [régression linéaire](https://fr.wikipedia.org/wiki/R%C3%A9gression_lin%C3%A9aire). Il est utilisé pour analyser les données issues des comportements passés, d'en extraire des motifs et de fournir des possibilités d'évolution des comportements à partir de la connaissance de ces motifs.

Par exemple, en me basant sur des millions de textes, j'apprends quel mot en suit un autre à 99%, vous comprenez maintenant comment Google arrive à prédire les mots que vous recherchez lorsque vous commencez à écrire ?

## 2. La classification

<img alt="Régression logistique" src="{{site.image_baseurl}}posts/logistic-regression.png" class="left-image" />Il s'agit d'un modèle mathématique de [régression logistique](https://fr.wikipedia.org/wiki/R%C3%A9gression_logistique). Il est utilisé afin de placer des individus dans des classes ; c'est à dire des ensembles qui partagent des caractères statistiques.

Par exemple une IA qui parcours des milliers d'images pour séparer les chats d'autres animaux est typiquement de type classification.

# Le Deep Learning - Apprentissage Profond

Il s'agit d'une discipline particulière au sein du Machine Learning utilisant notamment le réseau neuronal et certains modèles d'algorithmes afin de générer des modèles intelligents grâce à l'apprentissage. Contrairement aux autres méthodes, dans le deep learning, les données en entrées ne sont pas explicitement structurées ; c'est le réseau de neurones qui a pour but d'identifier au sein du jeu de données quelles sont les éléments importants et de les apprendre automatiquement.

<img alt="Detection de chat" src="{{site.image_baseurl}}posts/cat-classification.jpg" class="left-image" />
C'est ce qu'à fait [google](https://www.wired.com/2012/06/google-x-neural-network/) avec 16 000 processeurs et un milliard de connexions sur son service de vidéo Youtube. Il a été exposé à 10 million de vidéo sélectionnées au hasard ainsi qu'à 20 000 éléments différents pendant trois jours ; il a alors commencé à reconnaître des images de chats (il n'y a pas que sur Facebook que les images de chats foisonnent) bien qu'il n'ait reçu aucune information sur les caractéristiques distinctives qui pourraient l'aider à en identifier un.

Après seulement trois jours, le système a su détecter des visages humains à 81,7%, des parties de corps humain à 76,7% et des chats à 74,8%. A gauche, l'image du chat telle que l'IA se le représente à l'issue de ces 3 jours.

# Conclusion

L'IA nous offre de nombreuses possibilités toutes aussi merveilleuses les unes que les autres. Ces nouvelles possibilités touchent à des domaines aussi divers que la santé, les transports, l'imagerie, les textes, les langues, les jeux etc.

Dans mon prochain [article](/intelligence%20artificielle/2018/03/31/intelligence-artificielle-l-humain.html), je vais aborder l'IA d'AlphaGo en couvrant les dimensions techniques et humaines que cette IA a mis à jour. Comment ces artistes que sont les joueurs de GO ont vécu leurs défaites ? Qu'est ce que cette IA nous apporte ? Pourquoi est-elle si différente des autres à son heure ? Pour ne pas le manquer, vous n'avez qu'à suivre mon flux [rss](http://www.alain-andre.fr/feed.xml).

# Références

[elitedatascience.com](https://elitedatascience.com/keras-tutorial-deep-learning-in-python), [tensorflow.org](https://www.tensorflow.org/get_started/get_started), http://www.eskimoz.fr/machine-learning/, [Sylvain Peyronnet](https://www.youtube.com/watch?time_continue=140&v=EkoFySlZ4MY) responsable scientifique de Qwant., [Yann LeCun](https://www.youtube.com/watch?v=RgUcQceqC_Y) responsable du laboratoire sur l'IA de Facebook, [openclassrooms.com](https://openclassrooms.com/courses/initiez-vous-au-machine-learning), [theconnectedmag.fr](http://www.theconnectedmag.fr/intelligence-artificielle-imagerie-medicale/)
