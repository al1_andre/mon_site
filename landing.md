---
title: Mes articles références
layout: landing
description: Ci-dessous sont affichés des articles externes qui ont pour moi de l'importance.
image: preferred.jpg
nav-menu: true
---

<!-- Main -->
<div id="main">

  <section id="two" class="spotlights">
    <section>
  		<a href="https://blog.mozilla.org/blog/2017/11/29/announcing-the-initial-release-of-mozillas-open-source-speech-recognition-model-and-voice-dataset/" target="_blank" class="image">
  			<img src="{{ site.image_baseurl }}landing/MZ_CommonVoice_blog_post_HR-1-768x432.jpg" alt="Speech Recognition" data-position="middle center" />
  		</a>
  		<div class="content">
  			<div class="inner">
  				<header class="major">
  					<h3>Mozilla’s Open Source Speech Recognition Model and Voice Dataset</h3>
  				</header>
  				<p>There are only a few commercial quality speech recognition services available, dominated by a small number of large companies....This is why we started DeepSpeech as an open source project...</p>
  				<ul class="actions">
  					<li><a href="https://blog.mozilla.org/blog/2017/11/29/announcing-the-initial-release-of-mozillas-open-source-speech-recognition-model-and-voice-dataset/" target="_blank" class="button">En savoir plus</a></li>
  				</ul>
  			</div>
  		</div>
  	</section>

  	<section>
  		<a href="https://hacks.mozilla.org/2016/11/rust-and-the-future-of-systems-programming/" target="_blank" class="image">
  			<img src="{{ site.image_baseurl }}landing/mozilla-love-rust.png" alt="Mozilla love Rust" data-position="middle center" />
  		</a>
  		<div class="content">
  			<div class="inner">
  				<header class="major">
  					<h3>Rust and the Future of Systems Programming</h3>
  				</header>
  				<p>If you’re a regular reader of Hacks, you probably know about Rust, the ground-breaking, community-driven systems programming language sponsored by Mozilla...</p>
  				<ul class="actions">
  					<li><a href="https://hacks.mozilla.org/2016/11/rust-and-the-future-of-systems-programming/" target="_blank" class="button">En savoir plus</a></li>
  				</ul>
  			</div>
  		</div>
  	</section>
  	
  	<section>
  		<a href="https://chrisherring.co/posts/why-rails-is-still-worth-learning-in-2017" target="_blank" class="image">
  			<img src="{{ site.image_baseurl }}landing/rails-worth-it.jpg" alt="Rails worth it" data-position="middle center" />
  		</a>
  		<div class="content">
  			<div class="inner">
  				<header class="major">
  					<h3>Why Rails is still worth learning in 2017</h3>
  				</header>
  				<p>Rails has been around since 2004, lot's of developers are moving to Node and Elixir, but none of this stops Rails being an excellent choice for web development...</p>
  				<ul class="actions">
  					<li><a href="https://chrisherring.co/posts/why-rails-is-still-worth-learning-in-2017" target="_blank" class="button">En savoir plus</a></li>
  				</ul>
  			</div>
  		</div>
  	</section>
  </section>
  
</div>
